###
# Copyright (c) 2021, mogad0n and Georg Pfuetzenreuter <georg@lysergic.dev>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

from supybot import conf, registry, ircutils

try:
    from supybot.i18n import PluginInternationalization

    _ = PluginInternationalization("SnoParser")
except:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x

# class ValidChannel(registry.string):
#    """Value must be a valid channel"""
#    def setValue(self, v):
#        if not (ircutils.isChannel(v)):
#            self.error()
#        registry.String.setValue(self, v)


def configure(advanced):
    # This will be called by supybot to configure this module.  advanced is
    # a bool that specifies whether the user identified themself as an advanced
    # user or not.  You should effect your configuration by manipulating the
    # registry as appropriate.
    from supybot.questions import expect, anything, something, yn

    conf.registerPlugin("SnoParser", True)


SnoParser = conf.registerPlugin("SnoParser")
# This is where your configuration variables (if any) should go.  For example:
# conf.registerGlobalValue(SnoParser, 'someConfigVariableName',
#     registry.Boolean(False, _("""Help for someConfigVariableName.""")))

# conf.registerNetworkValue(SnoParser, 'targetChannel', ValidChannel,
#    ("", ("""Determines which channel the bot should send snolines""")))

conf.registerNetworkValue(
    SnoParser,
    "targetChannel",
    registry.String(
        "",
        ("""Determines which channel the bot should snolines example: `#snotices`"""),
    ),
)

conf.registerGlobalValue(
    SnoParser,
    "AutoVhost",
    registry.String(
        "libcasa/user/", ("""Configure the vhost eg. libcasa/user/$account""")
    ),
)

conf.registerGlobalValue(
    SnoParser,
    "preventHighlight",
    registry.Boolean(True, ("""Toggles in channel highlights with ZWSP""")),
)

conf.registerGlobalValue(
    SnoParser,
    "debug",
    registry.Boolean(
        "false",
        """
    SnoParser: Verbose output. Note: there is a seperate debug option for the `whois` client.
    """,
        private=True,
    ),
)

###
# REDIS related settings below:
###
conf.registerGroup(SnoParser, "redis")
conf.registerGroup(SnoParser.redis, "db")
conf.registerGlobalValue(
    SnoParser.redis.db,
    "ips",
    registry.Integer(
        2,
        """
    Redis: Database number for counting of IP ADDRESSES.
    """,
        private=True,
    ),
)
conf.registerGlobalValue(
    SnoParser.redis.db,
    "nicks",
    registry.Integer(
        1,
        """
    Redis: Database number for counting of NICKNAMES.
    """,
        private=True,
    ),
)
conf.registerGlobalValue(
    SnoParser.redis.db,
    "whois",
    registry.Integer(
        0,
        """
    Redis: Database number for WHOIS query caching.
    """,
    ),
)

conf.registerGlobalValue(
    SnoParser.redis,
    "host",
    registry.String(
        "localhost",
        """
    Redis: IP address or hostname.
    """,
        private=True,
    ),
)
conf.registerGlobalValue(
    SnoParser.redis,
    "port",
    registry.Integer(
        6379,
        """
    Redis: Port.
    """,
        private=True,
    ),
)
conf.registerGlobalValue(
    SnoParser.redis,
    "username",
    registry.String(
        "",
        """
    Redis: Username. This is optional and has not been tested. It is recommended to perform initial tests on a local instance with no username and password.
    """,
        private=True,
    ),
)
conf.registerGlobalValue(
    SnoParser.redis,
    "password",
    registry.String(
        "",
        """
    Redis: Password. This is optional and has not been tested. It is recommended to perform initial tests on a local instance with no username and password.
    """,
    ),
)
conf.registerGlobalValue(
    SnoParser.redis,
    "timeout",
    registry.Integer(
        5,
        """
    Redis: Socket Timeout. The developer does not know what to recommend here, but `5` seems to yield good results.
    """,
    ),
)


###
##  WHOIS related settings below:
###
conf.registerGroup(SnoParser, "whois")
conf.registerGlobalValue(
    SnoParser.whois,
    "debug",
    registry.Boolean(
        "false",
        """
    SnoParser: True: Very verbose console output. False: Mostly silent operation.
    """,
        private=True,
    ),
)
conf.registerGlobalValue(
    SnoParser.whois,
    "sample",
    registry.String(
        "",
        """
    SnoParser: This allows to set a testing IP address, if the plugin shall be evaluated on i.e. a local network. This will override all IP addresses from SNOTICES!
    """,
        private=True,
    ),
)
conf.registerGlobalValue(
    SnoParser.whois,
    "ttl",
    registry.Integer(
        3600,
        """
    SnoParser: How long to cache WHOIS entries for.
    """,
        private=True,
    ),
)

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
